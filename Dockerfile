FROM debian:stable-20240211 as runtime-image

# Use root account to use apt
USER root


# Install build dependencies
RUN apt update && \
    apt install -y --no-install-recommends \
        gnupg2 \
        apt-transport-https \
        wget \
        lsb-release \
        ca-certificates \
        git \
        curl



# Install Freeswitch deps
#ENV SIGNALWIRE_TOKEN=pat_v7qVfqdoqezrYTynyB5EQM7a
ENV SIGNALWIRE_TOKEN=pat_DqGy8QMSF4KXeEUQeceyQC5q

RUN wget --http-user=signalwire --http-password=$SIGNALWIRE_TOKEN -O /usr/share/keyrings/signalwire-freeswitch-repo.gpg https://freeswitch.signalwire.com/repo/deb/debian-release/signalwire-freeswitch-repo.gpg

RUN echo "machine freeswitch.signalwire.com login signalwire password $SIGNALWIRE_TOKEN" > /etc/apt/auth.conf &&\
    chmod 600 /etc/apt/auth.conf &&\
    echo "deb [signed-by=/usr/share/keyrings/signalwire-freeswitch-repo.gpg] https://freeswitch.signalwire.com/repo/deb/debian-release/ `lsb_release -sc` main" > /etc/apt/sources.list.d/freeswitch.list &&\
    echo "deb-src [signed-by=/usr/share/keyrings/signalwire-freeswitch-repo.gpg] https://freeswitch.signalwire.com/repo/deb/debian-release/ `lsb_release -sc` main" >> /etc/apt/sources.list.d/freeswitch.list &&\
    apt update
RUN apt build-dep -y freeswitch

# Then let's get the source. Use the -b flag to get a specific branch
COPY  .  /usr/src/

# ... and do the build
# The -j argument spawns multiple threads to speed the build process, but causes trouble on some systems

RUN cd /usr/src/freeswitch &&\
    chmod +x bootstrap.sh &&\
    chmod +x /usr/src/freeswitch/libs/apr/build/PrintPath &&\ 
    chmod +x libs/libvpx/configure &&\
    chmod +x /usr/src/freeswitch/libs/libvpx/build/make/version.sh &&\
    ./bootstrap.sh -j &&\
    ./configure &&\
    make &&\
    make install &&\
    make cd-moh-install &&\
    rm -rf /usr/src/freeswitch



# Install Freeswitch and modules
RUN apt update --allow-releaseinfo-change && \
    apt install --no-install-recommends -y \
                   libasound2 \
                   libasound2-data \
                   libbsd0 \
                   libcurl4 \
                   libedit2 \
                   libfreeswitch1 \
                   libgomp1 \
                   libgsm1 \
                   libltdl7 \
                   liblua5.2-0 \
                   libodbc1 \
                   libogg0 \
                   libogg-dev \
                   libopencore-amrnb0 \
                   libopencore-amrwb0 \
                   libpng16-16 \
                   libpq5 \
                   libsndfile1 \
                   libsox-fmt-alsa \
                   libsox-fmt-base \
                   libsox3 \
                   libspeex1 \
                   libspeexdsp1 \
                   libvorbis0a \
                   libvorbis-dev \
                   libvorbisenc2 \
                   libvorbisfile3 \
                   libwavpack1 \
                   # Additional
                   ca-certificates \
                   flac \
                   init-system-helpers \
                   sox \
                   msmtp \
                   flite \
                   git


RUN apt install -y sudo wget tar


# Add freeswitch user
RUN cd /usr/local &&\
    groupadd freeswitch &&\
    adduser --quiet --system --no-create-home --gecos "FreeSWITCH open source softswitch" --ingroup freeswitch freeswitch --disabled-password &&\
    chown -R freeswitch:freeswitch /usr/local/freeswitch/ &&\
    chmod -R ug=rwX,o= /usr/local/freeswitch/ &&\
    chmod -R u=rwx,g=rx /usr/local/freeswitch/bin/* &&\
    usermod -d /usr/local/freeswitch freeswitch &&\
    for binary in $(ls /usr/local/freeswitch/bin); do echo $binary; ln -sfn /usr/local/freeswitch/bin/$binary /usr/bin/$binary; chmod +x /usr/bin/$binary; done

WORKDIR /etc/freeswitch
COPY . .

ENTRYPOINT [ "/etc/freeswitch/init.sh", "freeswitch"]